/**
 * @file
 * Gulp file.
 */

var theme_base_path = 'web/themes/custom/';
var themes = {
  'theme_name': 'Theme name'
};
var options = {
  'deploy': {
    'code_dir': 'web/'
  },
  'themes': []
};

// Generate info for the themes.
for (var theme_name in themes) {
  var theme = {
    'title': themes[theme_name],
    'path': theme_base_path + theme_name + '/',
    'sass': {
      'files': [
        theme_base_path + theme_name + '/sass/**/*.scss',
        '!' + theme_base_path + theme_name + '/sass/generated/*'
      ],
      'destination': theme_base_path + theme_name + '/css/'
    },
    'styleguide': {
      'source': theme_base_path + theme_name + '/sass/',
      'builder': 'node_modules/michelangelo/kss_styleguide/custom-template/',
      'destination': 'styleguide/' + theme_name + '/',
      'title': themes[theme_name] + ' Living Style Guide'
    }
  };
  options['themes'].push(theme);
}

/*
Available tasks:
  'gulp'                      : Default task, runs `gulp:build`.
  'gulp build'                : Runs a complete development build of the system.
  'gulp build:deploy'         : Runs a complete deployment build of the system.
  'gulp compile:system'       : Runs `composer install`.
  'gulp compile:system:deploy': Runs `composer install` without dev dependencies.
  'gulp compile:sass'         : Compiles the sass files (with sourcemaps).
  'gulp compile:sass:deploy'  : Compiles the sass files (for deployment).
  'gulp deploy'               : Deploys to selected environment.
  'gulp lint'                 : Runs all the available linters.
  'gulp lint:php'             : Runs the Drupal PHP CS linter.
  'gulp lint:sass'            : Runs StyleLint.
  'gulp test:css'             : Runs Parker.
  'gulp watch'                : Starts a watch for all themes.
*/

/*
Used modules:
  gulp                 : The streaming build system.
  gulp-autoprefixer    : Prefix CSS.
  gulp-load-plugins    : Automatically load Gulp plugins.
  gulp-parker          : Stylesheet analysis tool.
  gulp-sass            : Compile Sass.
  gulp-sass-glob-import: Provide Sass Globbing.
  gulp-sourcemaps      : Generate sourcemaps.
  gulp-util            : Utility functions.
  husky                : Enable pre-commit hooks.
  kss                  : Living Style Guide Generator.
  run-sequence         : Run a series of dependent Gulp tasks in order.
*/

/*
Frontend dependencies:
  breakpoint-sass: Really Simple Media Queries with Sass.
  kss            : A methodology for documenting CSS and building style guides.
  node-sass      : Wrapper around libsass.
*/

// Global requirements.
var gulp = require('gulp');
var child_process = require('child_process');
var plugins = require('gulp-load-plugins')({
  pattern: '*',
  rename: {
    'gulp-exec': 'gexec',
    'gulp-util': 'gutil',
    'merge-stream': 'merge',
    'stylelint': 'sl'
  }
});

// Error report callback function.
var reportError = function (error) {
  var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

  plugins.notify({
    title: 'Task Failed [' + error.plugin + ']',
    message: lineNumber + 'See console.',
    sound: 'Blow' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
  }).write(error);

  plugins.gutil.beep(); // Beep 'sosumi' again.

  // Pretty error reporting.
  var report = '';
  var chalk = plugins.gutil.colors.white.bgRed;

  report += chalk('TASK:') + ' [' + error.plugin + ']\n';
  report += chalk('PROB:') + ' ' + error.message + '\n';
  if (error.lineNumber) {
    report += chalk('LINE:') + ' ' + error.lineNumber + '\n';
  }
  if (error.fileName) {
    report += chalk('FILE:') + ' ' + error.fileName + '\n';
  }
  console.error(report);

  // Prevent the 'watch' task from stopping.
  this.emit('end');
};

// Tasks.
require('./gulp/tasks/build')(gulp, plugins, options);
require('./gulp/tasks/compile-sass')(gulp, plugins, options, reportError);
require('./gulp/tasks/compile-styleguide')(gulp, plugins, options);
require('./gulp/tasks/compile-system')(gulp, plugins, options, child_process);
require('./gulp/tasks/default')(gulp, plugins, options);
require('./gulp/tasks/deploy')(gulp, plugins, options, child_process);
require('./gulp/tasks/lint')(gulp, plugins, options);
require('./gulp/tasks/lint-code')(gulp, plugins, options);
require('./gulp/tasks/lint-sass')(gulp, plugins, options);
require('./gulp/tasks/test-css')(gulp, plugins, options);
require('./gulp/tasks/watch')(gulp, plugins, options);

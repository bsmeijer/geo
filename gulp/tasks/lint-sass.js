/**
 * @file
 * Task: Lint: SASS.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('lint:sass', function () {
    var paths = [];
    options.themes.forEach(function (theme, index, array) {
      paths = paths.concat(theme.sass.files);
    });
    return gulp.src(paths)
      .pipe(plugins.stylelint({
        failAfterError: true,
        reporters: [
          { formatter: 'string', console: true }
        ]
    }));
  });

  gulp.task('lint:sass:withfix', function () {
    var paths = [];
    options.themes.forEach(function (theme, index, array) {
      paths = paths.concat(theme.sass.files);
    });
    return gulp.src(paths, { base: './' })
      .pipe(plugins.stylelint({
        fix: true,
        failAfterError: true,
        reporters: [
          { formatter: 'string', console: true }
        ]
      }))
      .pipe(gulp.dest('./'));
  });

};

/**
 * @file
 * Task: Lint.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('lint', ['lint:sass']);
};

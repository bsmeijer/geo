/**
 * @file
 * Task: Compile: Styleguide.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('compile:styleguide', function () {
    options.themes.forEach(function (theme, index, array) {
      return plugins.kss(theme.styleguide);
    });
  });
};

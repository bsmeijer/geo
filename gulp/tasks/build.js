/**
 * @file
 * Task: Build.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('build', [
    'compile:system',
    'compile:sass'
  ]);

  gulp.task('build:deploy', [
    'compile:system:deploy',
    'compile:sass:deploy'
  ]);
};

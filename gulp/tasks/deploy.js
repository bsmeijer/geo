/**
 * @file
 * Task: Deploy.
 */

// Global module.
module.exports = function (gulp, plugins, options, child_process) {
  'use strict';

  gulp.task('deploy' ,function () {
    var c = child_process,
        args = plugins.yargs.argv,
        yarg_env = args.env ? args.env : 'test',
        fs = require('fs'),
        path = process.cwd();

    return options.deploy.environment.forEach(function (environment, index, array) {
      if (environment.name === yarg_env) {
        var envfolder = path + '/' + environment.name;

        if (!fs.existsSync(environment.name)) {
          // It is a new dir/repository so first time mkdir and create.
          console.log('Make a new dir: ' + environment.name);
          fs.mkdirSync(environment.name);

          // Clone repository.
          console.log('Cloning: ' + environment.repository);
          c.execSync('git clone ' + environment.repository + ' .', {'cwd': envfolder});

          // Clone branch.
          console.log('Check out branch: ' + environment.branch);
          c.execSync('git checkout ' + environment.branch, {'cwd': envfolder});

        }
        console.log('Pull latest code for ' + environment.name);
        c.execSync('git pull', {'cwd': envfolder});
        console.log(envfolder);

        options.deploy.buildFiles.forEach(function (file, index, array) {
          gulp.src(file)
            .pipe(gulp.dest(envfolder));
        });

        return options.deploy.buildDirs.forEach(function (dir, index, array) {
          var rsync = 'rsync -rqtcv --chmod=Du+rwx --delete --delete-excluded ';

          if (typeof dir.keep_symlinks === 'undefined' || !dir.keep_symlinks) {
            rsync = rsync + ' -L ';
          }
          else {
            rsync = rsync + ' -l ';
          }

          if (typeof dir.exclude !== 'undefined') {
            dir.exclude.forEach(function (exclude, index, array) {
              rsync = rsync + ' --exclude=' + exclude + ' ';
            });
          }

          rsync = rsync + dir.name + '/ --exclude=.git ' + envfolder + '/' + dir.destination;
          console.log("====================== Start " + dir.name + " =====================");
          console.log("Command: " + rsync);
          c.execSync(rsync);
          console.log("====================== Finished " + dir.name + " =====================");
        });
      }
    });
  });
};

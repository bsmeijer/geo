/**
 * @file
 * Task: Watch.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('watch', ['watch:sass', 'watch:styleguide']);

  gulp.task('watch:sass', function () {
    options.themes.forEach(function (theme, index, array) {
      return gulp.watch(theme.sass.files, ['compile:sass']);
    });
  });

  gulp.task('watch:styleguide', function () {
    options.themes.forEach(function (theme, index, array) {
      return gulp.watch(theme.sass.files, ['compile:styleguide']);
    });
  });
};

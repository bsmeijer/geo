/**
 * @file
 * Task: Compile Sass files.
 */

// Global module.
module.exports = function (gulp, plugins, options, reportError) {
  'use strict';

  gulp.task('compile:sass', function (callback) {
    plugins.runSequence(
      'compile:sass:images:optimize',
      'compile:sass:images',
      'compile:sass:main',
      callback
    );
  });

  gulp.task('compile:sass:deploy', function (callback) {
    plugins.runSequence(
      'compile:sass:images',
      'compile:sass:main:deploy',
      callback
    );
  });

  gulp.task('compile:sass:main', function () {
    var merged = plugins.merge();

    options.themes.forEach(function (theme, index, array) {
      merged.add(gulp.src(theme.sass.files)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sassGlob())
        .pipe(plugins.sass({
          includePaths: ['./node_modules/breakpoint-sass/stylesheets'],
          errLogToConsole: true,
          outputStyle: 'expanded'
        }).on('error', reportError))
        .pipe(plugins.autoprefixer({
          cascade: false,
          add: true,
          remove: true,
          supports: true,
          flexbox: true,
          grid: true
        }))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(theme.sass.destination))
        .pipe(plugins.notify('<%= file.relative %> for theme `' + theme.title + '` compiled.')));
    });

    return merged;
  });

  gulp.task('compile:sass:main:deploy', function () {
    var merged = plugins.merge();

    options.themes.forEach(function (theme, index, array) {
      merged.add(gulp.src(theme.sass.files)
        .pipe(plugins.sassGlob())
        .pipe(plugins.sass({
          includePaths: ['./node_modules/breakpoint-sass/stylesheets'],
          errLogToConsole: true,
          outputStyle: 'expanded'
        }).on('error', reportError))
        .pipe(plugins.autoprefixer({
          cascade: false,
          add: true,
          remove: true,
          supports: true,
          flexbox: true,
          grid: true
        }))
        .pipe(gulp.dest(theme.sass.destination))
        .pipe(plugins.notify('<%= file.relative %> for theme `' + theme.title + '` compiled.')));
    });

    return merged;
  });

  // Optimize all image assets with caching for optimum performance.
  gulp.task('compile:sass:images:optimize', function () {
    var merged = plugins.merge();

    options.themes.forEach(function (theme, index, array) {
      // Not a very subtle way to go about it, but the caching system makes sure
      // we don't see any noticeable performance loss.
      plugins.del.sync(theme.path + 'img');
      merged.add(gulp.src(theme.path + 'imgsrc/**/*.+(jpg|png|gif|svg)')
        .pipe(plugins.cache(plugins.imagemin([
          plugins.imagemin.gifsicle({interlaced: true}),
          plugins.imagemin.jpegtran({progressive: true}),
          plugins.imagemin.optipng({optimizationLevel: 5}),
          plugins.imagemin.svgo({
            plugins: [
              {removeViewBox: false},
              {cleanupIDs: true}
            ]
          })
        ])))
        .pipe(gulp.dest(theme.path + 'img')));
    });

    return merged;
  });

  gulp.task('clear', () =>
    plugins.cache.clearAll()
  );

  // Compile all the images to a scss file so we have the choice to include
  // them inline in our css.
  gulp.task('compile:sass:images', function () {
    var merged = plugins.merge();

    options.themes.forEach(function (theme, index, array) {
      merged.add(gulp.src(theme.path + 'img/**/*.+(jpg|png|gif|svg)')
        .pipe(plugins.sassImage({
          targetFile: '_image-urls.scss',
          template: 'gulp/sass-image-url-template.mustache',
          images_path: theme.path + 'img/',
          css_path: theme.sass.destination,
          prefix: 'icon-'
        }))
        .pipe(gulp.dest(theme.path + 'sass/generated/')));
      merged.add(gulp.src(theme.path + 'img/widgets/**/*.+(jpg|png|gif|svg)')
        .pipe(plugins.sassImage({
          targetFile: '_image-inline.scss',
          template: 'gulp/sass-image-inline-template.mustache',
          images_path: theme.path + 'img/',
          css_path: theme.sass.destination,
          prefix: 'icon-'
        }))
        .pipe(gulp.dest(theme.path + 'sass/generated/')));
    });

    return merged;
  });
};

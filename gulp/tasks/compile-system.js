/**
 * @file
 * Task: Compile System.
 */

// Global module.
module.exports = function (gulp, plugins, options, child_process) {
  'use strict';

  gulp.task('compile:system', function () {
    plugins.runSequence(
      'compile:composer',
      'compile:updb',
      'compile:cr'
    );
  });

  gulp.task('compile:system:deploy', function () {
    plugins.runSequence(
      'compile:composer:deploy',
      'compile:updb',
      'compile:cr'
    );
  });

  gulp.task('compile:composer', function () {
    child_process.execSync('composer install');
  });

  gulp.task('compile:composer:deploy', function () {
    child_process.execSync('composer install --no-dev');
  });

  gulp.task('compile:updb', function (callback) {
    options = {
      cwd: options.deploy.code_dir
    }
    child_process.execSync('../vendor/bin/drush updb -y', options);
  });

  gulp.task('compile:cr', function () {
    options = {
      cwd: options.deploy.code_dir
    }
    child_process.execSync('../vendor/bin/drush cr', options);
  });
};

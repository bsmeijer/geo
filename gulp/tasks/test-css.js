/**
 * @file
 * Task: Test: CSS.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  gulp.task('test:css', function () {
    var merged = plugins.merge();

    options.themes.forEach(function (theme, index, array) {
      merged.add(gulp.src(theme.sass.destination + '*.css')
        .pipe(plugins.parker()));
    });

    return merged;
  });
};

/**
 * @file
 * Task: Default.
 */

// Global module.
module.exports = function (gulp, plugins, options) {
  'use strict';

  // The default task (called when you run `gulp` from cli).
  gulp.task('default', ['build']);
};

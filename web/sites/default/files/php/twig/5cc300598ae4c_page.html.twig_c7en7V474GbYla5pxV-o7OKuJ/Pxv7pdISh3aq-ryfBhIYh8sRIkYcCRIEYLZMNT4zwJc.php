<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/geonovum/templates/templates/layout/page.html.twig */
class __TwigTemplate_9df52956e3b1e7bb6a2085d794e68f28c5ebda61241f9f580174b959e16702d0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header role=\"banner\" class=\"page-header\">
  <div class=\"header\">
    <a class=\"menu-toggle\"><i class=\"fas fa-bars\"></i></a>
    ";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
    <a class=\"search-toggle\"><i class=\"fas fa-search\"></i></a>
  </div>
  <div class=\"navigation\">
    ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
  </div>
</header>

<main role=\"main\" class=\"page-content\">
  <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 14
        echo "  <div class=\"above\">
    ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "above", [])), "html", null, true);
        echo "
  </div>
  <div class=\"main-content-wrapper\">
    ";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
    ";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "aside", [])), "html", null, true);
        echo "
  </div>
</main>

<footer role=\"contentinfo\" class=\"page-footer\">
  <div class=\"footer\">
    <div class=\"container\">
      ";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
    </div>
  </div>
  <div class=\"disclaimer\">
    <div class=\"container\">
      <div class=\"copyright\"><a href=\"https://geonovum.nl\">&copy; 2019 Geonovum</a></div>
        ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "disclaimer", [])), "html", null, true);
        echo "
      <div>
    </div>
  </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/geonovum/templates/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 32,  98 => 26,  88 => 19,  84 => 18,  78 => 15,  75 => 14,  67 => 8,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<header role=\"banner\" class=\"page-header\">
  <div class=\"header\">
    <a class=\"menu-toggle\"><i class=\"fas fa-bars\"></i></a>
    {{ page.header }}
    <a class=\"search-toggle\"><i class=\"fas fa-search\"></i></a>
  </div>
  <div class=\"navigation\">
    {{ page.navigation }}
  </div>
</header>

<main role=\"main\" class=\"page-content\">
  <a id=\"main-content\" tabindex=\"-1\"></a>{# link is in html.html.twig #}
  <div class=\"above\">
    {{ page.above }}
  </div>
  <div class=\"main-content-wrapper\">
    {{ page.content }}
    {{ page.aside }}
  </div>
</main>

<footer role=\"contentinfo\" class=\"page-footer\">
  <div class=\"footer\">
    <div class=\"container\">
      {{ page.footer }}
    </div>
  </div>
  <div class=\"disclaimer\">
    <div class=\"container\">
      <div class=\"copyright\"><a href=\"https://geonovum.nl\">&copy; 2019 Geonovum</a></div>
        {{ page.disclaimer }}
      <div>
    </div>
  </div>
</footer>
", "themes/custom/geonovum/templates/templates/layout/page.html.twig", "/var/www/html/web/themes/custom/geonovum/templates/templates/layout/page.html.twig");
    }
}

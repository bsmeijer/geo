<?php

namespace Drupal\mdsnippets;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\node\Entity\Node;

/**
 * Class Importer.
 *
 * @package Drupal\mdsnippets
 */
class Importer {

  protected $configuration;
  protected $cacheBackend;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend) {
    $this->configuration = $config_factory->get('mdsnippets.config');
    $this->cacheBackend = $cache_backend;
  }

  public function update() {
    $lock = \Drupal::lock();
    if (!$lock->acquire('mdsnippets_update')) {
      $this->log('Trying to start update while import is already in progress.');
      $lock->release('mdsnippets_update');
      return FALSE;
    }

    if (!$this->ensureGitRepository()) {
      $this->log('Unable to clone git repository.');
      $lock->release('mdsnippets_update');
      return FALSE;
    }

    $commits = $this->exec('git pull');

    $revision = \Drupal::state()->get('mdsnippets.revision');
    // $revision = '8cf33aa588fa2db91299ad72a7c8d33c667c2298';
    // $revision = '288015aa1ef428b0bf233b89a8990ebf65d8c862';
    $range = $revision ? "$revision..HEAD" : 'HEAD';
    $commits = $this->exec("git rev-list $range");
    $nextCommit = reset(array_reverse($commits));
    if (!$nextCommit) {
      $this->log('No new commit found');
      $lock->release('mdsnippets_update');
      return TRUE;
    }

    $this->processCommit($nextCommit);

    \Drupal::state()->set('mdsnippets.revision', $nextCommit);

    $lock->release('mdsnippets_update');

    return TRUE;
  }

  protected function processCommit($commit) {
    $this->log("Importing commit $commit");
    $files = $this->exec("git diff-tree --no-commit-id --name-only -r $commit");
    $md_files = array_filter($files, function($filename) {
      return preg_match('/\.md$/', $filename);
    });
    foreach ($md_files as $file) {
      $file_escaped = escapeshellarg($file);
      $contents = $this->exec("git show $commit:$file_escaped", FALSE);
      $this->processFile($file, $contents, $commit);
    }
    $media_files = array_filter($files, function($filename) {
      return preg_match('/\.(jpe?g|png|pdf|gif|docx?|xlsx?|pptx?)$/', $filename);
    });
    foreach ($media_files as $file) {
      $this->saveMedia($file);
    }
  }

  protected function splitTitleAndBody($contents, $default_title = 'Untitled') {
    $title = $default_title;
    if (preg_match('/^(.+).\r?\n-+.\r?\n/s', $contents, $match)) {
      $title = trim($match[1]);
      $contents = substr($contents, strlen($match[0]));
    }
    return [$title, $contents];
  }

  protected function saveMedia($file) {
    $path = 'public://media';
    $realpath = \Drupal::service('file_system')->realpath($path);
    if (is_file($realpath . '/' . basename($file))) {
      return TRUE;
    }
    if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
      return FALSE;
    }
    $repository_path = $this->repositoryPath();
    if (!@copy("$repository_path/$file", \Drupal::service('file_system')->realpath($path) . '/' . basename($file))) {
      return FALSE;
    }
    // @todo: Retrieve public file path.
    return '/sites/default/files/media/' . basename($file);
  }

  protected function preprocessContent($content) {
    // Process links to local files.
    preg_match_all('/\\[([^\\]]*)\\]\\(([^)]+)\\)/', $content, $matches);
    for ($i = 0; $i < count($matches[0]); ++$i) {
      $tag = $matches[0][$i];
      $text = $matches[1][$i];
      $location = $matches[2][$i];
      if (preg_match('/^[\w\\/].+\\.(jpe?g|png|pdf|gif|docx?|xlsx?|pptx?)$/')) {
        $local_path = $this->saveMedia($location);
        if (!$local_path) {
          // Images can be committed later.
          // @todo: Retrieve public file path.
          $local_path = preg_replace('/^media\\//', '/sites/default/files/media/', $location);
        }
        $location = $local_path;
        $content = str_replace($tag, "[$text]($local_path)", $content);
      }
    }

    // Optimistic replacement of media paths.
    // Required for files that will be committed later.
    $content = str_replace('](media/', '](/sites/default/files/media/', $content);

    return $content;
  }

  protected function processFile($file, $contents, $commit) {
    $title = preg_replace('/\.md$/', '', basename($file));
    $contents = $this->preprocessContent($contents);
    $body = [
      'value' => $contents,
      'format' => 'markdown',
    ];

    $entity_id = db_select('mdsnippets', 's')->fields('s', ['entity_id'])->condition('filename', $file)->execute()->fetchField();
    if ($entity_id) {
      $entity = Node::load($entity_id);
      $entity->set('title', $title);
      $entity->set('body', $body);
      $entity->set('status', empty($contents) ? 0 : 1);
      $entity->save();
    }
    else {
      $entity = Node::create([
        'type' => 'snippet',
        'title' => basename($file),
        'body' => $body,
        'uid' => 1,
        'status' => 1,
      ]);
      $entity->save();
      db_insert('mdsnippets')->fields([
        'entity_id' => $entity->id(),
        'filename' => $file,
      ])->execute();
    }
  }

  protected function exec($command, $split_lines = TRUE) {
    $path = $this->repositoryPath();
    $output = shell_exec("cd $path && $command");
    if ($split_lines) {
      $output = array_filter(array_map('trim', explode("\n", $output)));
    }
    return $output;
  }

  protected function repositoryPath() {
    $path = $this->configuration->get('repositoryLocation');
    return \Drupal::service('file_system')->realpath($path);
  }

  protected function ensureGitRepository() {
    $path = $this->repositoryPath();
    $respository = $this->configuration->get('repository');
    if (!is_file("$path/.git/HEAD")) {
      // Repository does not exist. Try to clone repository.
      shell_exec("git clone $respository $path");
      return is_file("$path/.git/HEAD");
    }
    return TRUE;
  }

  protected function log($message) {
    drupal_set_message(print_r($message, true));
  }


}
